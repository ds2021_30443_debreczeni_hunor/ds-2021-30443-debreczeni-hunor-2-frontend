import React from 'react';
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';
import PersonForm from "./components/person-form";

import * as API_USERS from "../../api/person-api"
import PersonTable from "./components/person-table";
import {NotificationManager} from 'react-notifications';

class UserContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reload = this.reload.bind(this);
        this.editUser = this.editUser.bind(this);
        this.deleteUser = this.deleteUser.bind(this);
        this.state = {
            userData: {},
            selected: false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
        this.fetchPersons();
    }

    fetchPersons() {
        API_USERS.getPersons().then(result => {
            this.setState({
                tableData: result,
                isLoaded: true
            });
        }).catch(error => {
            NotificationManager.error('Request to get people failed', error.status + " " + error.statusText, 5000);
        });
    }

    toggleForm(reset = true) {
        this.setState({selected: !this.state.selected});
        if (reset) {
            this.setState({
                userData: {}
            })
        }
    }

    reload(toggleForm = true) {
        this.setState({
            isLoaded: false
        });
        if (toggleForm) {
            this.toggleForm();
        }
        this.fetchPersons();
    }

    deleteUser(userId){
        API_USERS.deleteUser(userId).then(() => {
            NotificationManager.info(null, "Successfully deleted", 5000);
            this.reload(false);
        }).catch(error => {
            NotificationManager.error('Request to delete user failed', error.status + " " + error.statusText, 5000);
        })
    }

    editUser(user){
        this.setState({
            userData: user
        });
        this.toggleForm(false);
    }

    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Person Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '12', offset: 0}}>
                            <Button color="primary" onClick={this.toggleForm}>Add Person </Button>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '12', offset: 0}}>
                            {this.state.isLoaded && <PersonTable
                                deleteHandler={this.deleteUser}
                                editHandler={this.editUser}
                                tableData = {this.state.tableData}
                                props = {this.props}
                            />}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                                            errorStatus={this.state.errorStatus}
                                                            error={this.state.error}
                                                        />   }
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Add Person: </ModalHeader>
                    <ModalBody>
                        <PersonForm data={() => this.state.userData} reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>

            </div>
        )

    }
}


export default UserContainer;
