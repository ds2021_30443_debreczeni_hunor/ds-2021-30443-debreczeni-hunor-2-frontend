import React from "react";
import Table from "../../../commons/tables/table";
import {Link} from "react-router-dom";
import {Container} from "reactstrap";
import Button from "react-bootstrap/Button";

class PersonTable extends React.Component {

    columns = [
        {
            Header: "ID",
            accessor: "id",
        },
        {
            Header: 'Name',
            accessor: 'name',
        },
        {
            Header: "Email",
            accessor: "email",
        },
        {
            Header: "Address",
            accessor: "address",
        },
        {
            Header: 'Age',
            accessor: 'age',
        },
        {
            Header: 'Roles',
            accessor: 'roles',
        },
        {
            Header: 'Actions',
            Cell: ({original}) => (
                this.getCell(original)
            )
        }
    ];

    filters = [
        {
            accessor: 'id',
        },
        {
            accessor: 'name',
        },
        {
            accessor: 'email',
        },
        {
            accessor: 'address',
        },
        {
            accessor: 'age',
        },
        {
            accessor: 'roles',
        }
    ];

    constructor(props) {
        super(props);

        const query = new URLSearchParams(this.props.props.location.search);
        this.filters.forEach(filter => {
            let value = query.get(filter.accessor);
            if (value !== undefined) {
                filter['value'] = value;
            }
        })

        this.state = {
            tableData: this.props.tableData,
        };

        this.deleteHandler = this.props.deleteHandler;
        this.editHandler = this.props.editHandler;

        this.delete = this.delete.bind(this);
        this.edit = this.edit.bind(this);
    }

    getCell(original) {
        return <Container>
            <Link to={"/devices?userEmail=" + original.email} className="btn btn-outline-primary mr-1">Devices
                ({original.devices.length})</Link>
            <Button className={"btn btn-primary mr-1"} onClick={() => this.edit(original)}>Edit</Button>
            <Button className={"btn btn-danger"} onClick={() => this.delete(original)}>Delete</Button>
        </Container>
    }

    delete(data) {
        this.deleteHandler(data.id);
    }

    edit(data) {
        this.editHandler(data);
    }

    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={this.columns}
                search={this.filters}
                pageSize={5}
            />
        )
    }
}

export default PersonTable;