import React from 'react';
import validate from "./validators/person-validators";
import Button from "react-bootstrap/Button";
import * as API_USERS from "../../../api/person-api";
import APIResponseErrorMessage from "../../../commons/errorhandling/api-response-error-message";
import {Col, FormGroup, Input, Label, Row} from "reactstrap";
import {NotificationManager} from "react-notifications";
import Select from 'react-select'

const roleOptions = [
    {value: 'ROLE_ADMIN', label: 'ROLE_ADMIN'},
    {value: 'ROLE_USER', label: 'ROLE_USER'},
    {value: 'ROLE_DEVICE_EDITOR', label: 'ROLE_DEVICE_EDITOR'},
    {value: 'ROLE_SENSOR_EDITOR', label: 'ROLE_SENSOR_EDITOR'},
]

class PersonForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandler;

        this.state = {
            data: this.props.data,
            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                id: {
                    value: null,
                    valid: true
                },
                name: {
                    value: '',
                    placeholder: 'What is your name?...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                email: {
                    value: '',
                    placeholder: 'Email...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        emailValidator: true
                    }
                },
                password: {
                    value: '',
                    placeholder: '',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 6,
                        isRequired: true
                    }
                },
                age: {
                    value: '',
                    placeholder: 'Age...',
                    valid: false,
                    touched: false,
                },
                address: {
                    value: '',
                    placeholder: 'Cluj, Zorilor, Str. Lalelelor 21...',
                    valid: false,
                    touched: false,
                },
                roles: {
                    value: [],
                    valid: true,
                    touched: false,
                },
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.selectOption = this.selectOption.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    registerPerson(person) {
        API_USERS.postPerson(person).then(result => {
            console.log("Successfully inserted person with id: " + result);
            this.reloadHandler();
        }).catch(error => {
            error.json().then(data => {
                NotificationManager.error(data.details[0], data.status + " " + data.message, 5000);
            })
        });
    }

    updatePerson(person) {
        API_USERS.updatePerson(person).then(result => {
            console.log("Successfully updated person with id: " + result);
            this.reloadHandler();
        }).catch(error => {
            error.json().then(data => {
                NotificationManager.error(data.details[0], data.status + " " + data.message, 5000);
            })
        });
    }

    handleSubmit() {
        let person = {
            id: this.state.formControls.id.value,
            name: this.state.formControls.name.value,
            email: this.state.formControls.email.value,
            age: this.state.formControls.age.value,
            address: this.state.formControls.address.value,
            password: this.state.formControls.password.value,
            roles: this.state.formControls.roles.value.map(o => o.value)
        };

        if (person.id !== null) {
            this.updatePerson(person);
        } else {
            this.registerPerson(person);
        }
    }

    componentDidUpdate(prevProps, prevState) {
        let data = this.props.data();
        if (data !== undefined && data.id !== this.state.formControls.id.value) {
            let found = false;
            let currentData = this.state;
            for (const [key, value] of Object.entries(data)) {
                let keyValue = currentData['formControls'][key];
                if (keyValue !== undefined && keyValue['value'] !== value && value !== null) {
                    if (key === 'roles') {
                        keyValue['value'] = value.map(o => ({
                            value: o,
                            label: o
                        }))
                    } else {
                        keyValue['value'] = value;
                    }
                    keyValue['valid'] = true;
                    found = true;
                }
            }

            if (found) {
                this.setState(currentData);
            }
        }
    }

    selectOption(option) {
        console.log(option)
        let formData = this.state.formControls;
        formData['roles']['value'] = option;
        formData['roles']['valid'] = option.length !== 0;
        this.setState(formData)
    }

    render() {
        return (
            <div>

                <FormGroup id='id'>
                    <Label for='idField'>ID: </Label>
                    <Input name='id' id='idField'
                           defaultValue={this.state.formControls.id.value}
                           disabled
                    />
                </FormGroup>

                <FormGroup id='name'>
                    <Label for='nameField'> Name: </Label>
                    <Input name='name' id='nameField' placeholder={this.state.formControls.name.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.name.value}
                           touched={this.state.formControls.name.touched ? 1 : 0}
                           valid={this.state.formControls.name.valid}
                           required
                    />
                    {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
                    <div className={"error-message row"}> * Name must have at least 3 characters </div>}
                </FormGroup>

                <FormGroup id='email'>
                    <Label for='emailField'> Email: </Label>
                    <Input name='email' id='emailField' placeholder={this.state.formControls.email.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.email.value}
                           touched={this.state.formControls.email.touched ? 1 : 0}
                           valid={this.state.formControls.email.valid}
                           required
                    />
                    {this.state.formControls.email.touched && !this.state.formControls.email.valid &&
                    <div className={"error-message"}> * Email must have a valid format</div>}
                </FormGroup>

                <FormGroup id='password'>
                    <Label for='passwordField'> Password: </Label>
                    <Input name='password' id='passwordField' placeholder={this.state.formControls.password.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.password.value}
                           touched={this.state.formControls.password.touched ? 1 : 0}
                           valid={this.state.formControls.password.valid}
                           type={'password'}
                           required
                    />
                    {this.state.formControls.password.touched && !this.state.formControls.password.valid &&
                    <div className={"error-message"}> * Password must be at least of size 6</div>}
                </FormGroup>

                <FormGroup id='address'>
                    <Label for='addressField'> Address: </Label>
                    <Input name='address' id='addressField' placeholder={this.state.formControls.address.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.address.value}
                           touched={this.state.formControls.address.touched ? 1 : 0}
                           valid={this.state.formControls.address.valid}
                           required
                    />
                </FormGroup>

                <FormGroup id='age'>
                    <Label for='ageField'> Age: </Label>
                    <Input name='age' id='ageField' placeholder={this.state.formControls.age.placeholder}
                           min={0} max={100} type="number"
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.age.value}
                           touched={this.state.formControls.age.touched ? 1 : 0}
                           valid={this.state.formControls.age.valid}
                           required
                    />
                </FormGroup>
                <FormGroup id='roles'>
                    <Label for='ageField'> Roles: </Label>
                    <Select
                        options={roleOptions}
                        isMulti
                        className="basic-multi-select"
                        classNamePrefix="select"
                        onChange={this.selectOption}
                        value={this.state.formControls.roles.value}
                    />
                </FormGroup>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid}
                                onClick={this.handleSubmit}> Submit </Button>
                    </Col>
                </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }
            </div>
        );
    }
}

export default PersonForm;
