import React from 'react';
import {ErrorMessage, Field, Form, Formik} from 'formik';
import * as Yup from 'yup';
import {withRouter} from "react-router-dom";

import {authenticationService, socketService} from '../../services';
import {NotificationManager} from "react-notifications";

const LoginPage = (props) => {

    let token = authenticationService.getToken();
    if (token) {
        props.history.push("/");
    }

    return (
        <div>
            <div className="alert alert-info">
                Username: admin<br/>
                Password: test
            </div>
            <h2>Login</h2>
            <Formik
                initialValues={{
                    username: '',
                    password: ''
                }}
                validationSchema={Yup.object().shape({
                    username: Yup.string().required('Username is required'),
                    password: Yup.string().required('Password is required')
                })}
                onSubmit={({username, password}, {setStatus, setSubmitting}) => {

                    setStatus();
                    authenticationService.login(username, password)
                        .then(
                            user => {
                                socketService.connectSocket().then(result => {
                                    socketService.subscribe("/user/" + result.headers['user-name'] + "/queue/notification", response => {
                                        NotificationManager.info("Sensor " + JSON.parse(response.body).sensor_id + " measured a value over the peak", "Energy usage over peak");
                                    })
                                }).catch(error => {
                                    console.error(error)
                                })
                                props.history.push("/devices");
                            },
                            error => {
                                console.log(error);
                                setSubmitting(false);
                            }
                        );
                }}
                render={({errors, status, touched, isSubmitting}) => (
                    <Form>
                        <div className="form-group">
                            <label htmlFor="username">Username</label>
                            <Field name="username" type="text"
                                   className={'form-control' + (errors.username && touched.username ? ' is-invalid' : '')}/>
                            <ErrorMessage name="username" component="div" className="invalid-feedback"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="password">Password</label>
                            <Field name="password" type="password"
                                   className={'form-control' + (errors.password && touched.password ? ' is-invalid' : '')}/>
                            <ErrorMessage name="password" component="div" className="invalid-feedback"/>
                        </div>
                        <div className="form-group">
                            <button type="submit" className="btn btn-primary" disabled={isSubmitting}>Login</button>
                            {isSubmitting &&
                            <img alt={""}
                                 src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA=="/>
                            }
                        </div>
                        {status &&
                        <div className={'alert alert-danger'}>{status}</div>
                        }
                    </Form>
                )}
            />
        </div>
    )
}

export default withRouter(LoginPage);