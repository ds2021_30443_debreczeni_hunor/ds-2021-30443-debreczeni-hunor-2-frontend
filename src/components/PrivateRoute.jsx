import React, {Component} from 'react';
import {Redirect, Route, withRouter} from 'react-router-dom';
import PropTypes from 'prop-types';
import {authenticationService} from '../services';

class PrivateRoute extends Component {
    state = {
        hasAccess: false,
        loaded: false,
    }

    componentDidMount() {
        this.checkAccess();
    }

    checkAccess = () => {
        const {userRole, history} = this.props;

        // your fetch request

        authenticationService.checkRole(userRole)
            .then(hasAccess => {
                this.setState({
                    hasAccess: hasAccess,
                    loaded: true,
                });
            })
            .catch((err) => {
                authenticationService.logout()
                history.push('/');
            });
    }

    render() {
        const {component: Component, ...rest} = this.props;
        const {loaded, hasAccess} = this.state;
        if (!loaded) return null;
        return (
            <Route
                {...rest}
                render={props => {
                    return hasAccess ? (
                        <Component {...props} />
                    ) : (
                        <Redirect
                            to={{
                                pathname: '/',
                            }}
                        />
                    );
                }}
            />
        );
    }
}

export default withRouter(PrivateRoute);

PrivateRoute.propTypes = {
    userRole: PropTypes.string.isRequired,
};