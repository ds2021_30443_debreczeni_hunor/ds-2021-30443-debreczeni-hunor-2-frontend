import React from 'react';
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Button, Card, CardHeader, Col, Container, Modal, ModalBody, ModalHeader, Row} from 'reactstrap';
import SensorForm from "./components/sensor-form";

import * as API_SENSOR from "../../api/sensor-api"
import SensorTable from "./components/sensor-table";
import {NotificationManager} from 'react-notifications';
import {authenticationService} from "../../services";

class SensorContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reload = this.reload.bind(this);
        this.editUser = this.editUser.bind(this);
        this.deleteUser = this.deleteUser.bind(this);
        this.state = {
            selected: false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null,
            isAdmin: authenticationService.isAdmin()
        };
    }

    componentDidMount() {
        this.fetchPersons();
    }

    fetchPersons() {
        API_SENSOR.getSensors().then(result => {
            this.setState({
                tableData: result,
                isLoaded: true
            });
        }).catch(error => {
            NotificationManager.error('Request to get sensors failed', error.status + " " + error.statusText, 5000);
        });
    }

    toggleForm(reset = true) {
        this.setState({selected: !this.state.selected});
        if (reset) {
            this.setState({
                userData: {}
            })
        }
    }

    reload(toggleForm = true) {
        this.setState({
            isLoaded: false
        });
        if (toggleForm) {
            this.toggleForm(false);
        }
        this.fetchPersons();
    }

    deleteUser(userId) {
        API_SENSOR.deleteSensor(userId).then(() => {
            NotificationManager.info(null, "Successfully deleted", 5000);
            this.reload(false);
        }).catch(error => {
            NotificationManager.error('Request to delete sensor failed', error.status + " " + error.statusText, 5000);
        })
    }

    editUser(user) {
        this.setState({
            userData: user
        });
        this.toggleForm(false);
    }

    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Sensor Management </strong>
                </CardHeader>
                <Card>
                    {
                        this.state.isAdmin &&
                            <Container>
                                <br/>
                                <Row>
                                    <Col sm={{size: '12', offset: 0}}>
                                        <Button color="primary" onClick={this.toggleForm}>Add Sensor </Button>
                                    </Col>
                                </Row>
                            </Container>
                    }
                    <br/>
                    <Row>
                        <Col sm={{size: '12', offset: 0}}>
                            {this.state.isLoaded && <SensorTable
                                deleteHandler={this.deleteUser}
                                editHandler={this.editUser}
                                tableData={this.state.tableData}
                                props={this.props}
                            />}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />}
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Add Sensor: </ModalHeader>
                    <ModalBody>
                        <SensorForm userOptions={this.userOptions} data={() => this.state.userData}
                                    reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>

            </div>
        )

    }
}


export default SensorContainer;
