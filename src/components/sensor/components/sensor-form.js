import React from 'react';
import validate from "./validators/sensor-validator";
import Button from "react-bootstrap/Button";
import APIResponseErrorMessage from "../../../commons/errorhandling/api-response-error-message";
import {Col, FormGroup, Input, Label, Row} from "reactstrap";
import {NotificationManager} from "react-notifications";
import AsyncSelect from 'react-select/async';

import * as API_DEVICE from "../../../api/device-api"
import * as API_SENSOR from "../../../api/sensor-api"

class SensorForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandler;

        this.state = {
            data: this.props.data,
            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                id: {
                    value: null,
                    valid: true
                },
                name: {
                    value: '',
                    placeholder: 'Sensor name...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                deviceId: {
                    value: null,
                    valid: false,
                    touched: false,
                },
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.selectOption = this.selectOption.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    registerSensor(person) {
        API_SENSOR.postSensor(person).then(result => {
            console.log("Successfully inserted sensor with id: " + result);
            this.reloadHandler();
        }).catch(error => {
            error.json().then(data => {
                NotificationManager.error(data.details[0], data.status + " " + data.message, 5000);
            })
        });
    }

    updateSensor(person) {
        API_SENSOR.updateSensor(person).then(result => {
            console.log("Successfully updated sensor with id: " + result);
            this.reloadHandler();
        }).catch(error => {
            error.json().then(data => {
                console.error(data)
                // NotificationManager.error(data.details[0], data.status + " " + data.message, 5000);
            })
        });
    }

    handleSubmit() {
        let person = {
            id: this.state.formControls.id.value,
            name: this.state.formControls.name.value,
            deviceId: this.state.formControls.deviceId.value.value
        };

        if (person.id !== null) {
            this.updateSensor(person);
        } else {
            this.registerSensor(person);
        }
    }

    componentDidUpdate(prevProps, prevState) {
        let data = this.props.data();
        if (data !== undefined && data.id !== this.state.formControls.id.value) {
            let found = false;
            let currentData = this.state;
            for (const [key, value] of Object.entries(data)) {
                let keyValue = currentData['formControls'][key];
                if (keyValue !== undefined && keyValue['value'] !== value && value !== null) {
                    if (key === 'deviceId') {
                        keyValue['value'] = {
                            value: value,
                            label: data['deviceName']
                        }
                    } else {
                        keyValue['value'] = value;
                    }
                    keyValue['valid'] = true;
                    found = true;
                }
            }

            if (found) {
                this.setState(currentData);
            }
        }
    }

    selectOption(option) {
        console.log(option);
        let formData = this.state.formControls;
        formData['deviceId']['value'] = option;
        formData['deviceId']['valid'] = option !== "";
        this.setState({
            formControls: formData,
            formIsValid: option !== "" && formData.name.valid
        })
    }

    loadOptions(inputValue, callback) {
        API_DEVICE.getDevices().then(result => {
            callback(result.map(o => ({
                value: o.id,
                label: o.name
            })));
        });
    };

    render() {
        return (
            <div>

                <FormGroup id='id'>
                    <Label for='idField'>ID: </Label>
                    <Input name='id' id='idField'
                           defaultValue={this.state.formControls.id.value}
                           disabled
                    />
                </FormGroup>

                <FormGroup id='name'>
                    <Label for='nameField'> Name: </Label>
                    <Input name='name' id='nameField' placeholder={this.state.formControls.name.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.name.value}
                           touched={this.state.formControls.name.touched ? 1 : 0}
                           valid={this.state.formControls.name.valid}
                           required
                    />
                    {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
                    <div className={"error-message row"}> * Name must have at least 3 characters </div>}
                </FormGroup>

                <FormGroup id='device'>
                    <Label for='deviceField'> Device: </Label>
                    <AsyncSelect
                        onChange={this.selectOption}
                        isClearable={false}
                        isSearchable={false}
                        loadOptions={this.loadOptions}
                        value={this.state.formControls.deviceId.value}
                        cacheOptions
                        defaultOptions
                    />
                </FormGroup>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid}
                                onClick={this.handleSubmit}> Submit </Button>
                    </Col>
                </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }
            </div>
        );
    }
}

export default SensorForm;
