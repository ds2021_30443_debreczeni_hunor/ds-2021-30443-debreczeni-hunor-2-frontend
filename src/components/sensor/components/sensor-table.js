import React from "react";
import Table from "../../../commons/tables/table";
import {Link} from "react-router-dom";
import {Container} from "reactstrap";
import Button from "react-bootstrap/Button";
import {authenticationService} from "../../../services";

class SensorTable extends React.Component {

    columns = [
        {
            Header: "ID",
            accessor: "id",
        },
        {
            Header: 'Name',
            accessor: 'name',
        },
        {
            Header: "deviceName",
            Cell: ({original}) => (
                <Link to={"/devices?name=" + original.deviceName} className="btn btn-outline-primary mr-1">{original.deviceName}</Link>
            )
        },
        {
            Header: 'Actions',
            Cell: ({original}) => (
                this.getCell(original)
            )
        }
    ];

    filters = [
        {
            accessor: 'id',
        },
        {
            accessor: 'name',
        },
        {
            accessor: 'deviceName',
        }
    ];

    constructor(props) {
        super(props);

        const query = new URLSearchParams(this.props.props.location.search);
        this.filters.forEach(filter => {
            let value = query.get(filter.accessor);
            if (value !== undefined) {
                filter['value'] = value;
            }
        })

        this.state = {
            tableData: this.props.tableData,
            isAdmin: authenticationService.isAdmin()
        };

        this.deleteHandler = this.props.deleteHandler;
        this.editHandler = this.props.editHandler;

        this.delete = this.delete.bind(this);
        this.edit = this.edit.bind(this);
    }

    getCell(original) {
        return this.state.isAdmin && <Container>
            <Button className={"btn btn-primary mr-1"} onClick={() => this.edit(original)}>Edit</Button>
            <Button className={"btn btn-danger"} onClick={() => this.delete(original)}>Delete</Button>
        </Container>
    }

    delete(data) {
        this.deleteHandler(data.id);
    }

    edit(data) {
        this.editHandler(data);
    }

    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={this.columns}
                search={this.filters}
                pageSize={5}
            />
        )
    }
}

export default SensorTable;