import React from 'react';
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Button, Card, CardHeader, Col, Modal, ModalBody, ModalHeader, Row} from 'reactstrap';
import DeviceForm from "./components/device-form";

import * as API_DEVICE from "../../api/device-api"
import * as API_USERS from "../../api/person-api";
import * as API_MONITORED_DATA from "../../api/monitored-data-api";
import DeviceTable from "./components/device-table";
import {NotificationManager} from 'react-notifications';
import {authenticationService} from "../../services";
import {Container} from "react-bootstrap";
import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";
import { Bar } from 'react-chartjs-2';

class DeviceContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.randomizeData = this.randomizeData.bind(this);
        this.reload = this.reload.bind(this);
        this.editUser = this.editUser.bind(this);
        this.deleteUser = this.deleteUser.bind(this);
        let isAdmin = authenticationService.isAdmin();
        this.state = {
            userOptions: [],
            selected: false,
            date: null,
            selectedDevice: null,
            deviceUsage: 0,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null,
            isAdmin: isAdmin,
            data: null
        };

        if (isAdmin) {
            API_USERS.getPersons().then(result => {
                this.state.userOptions = result.map(o => ({
                    value: o.id,
                    label: o.email
                }))
            });
        }


        this.showUsage = this.showUsage.bind(this);
        this.dateChange = this.dateChange.bind(this);
    }

    componentDidMount() {
        this.fetchPersons();
        this.randomizeData()
    }

    randomizeData(){
        let data = {
            labels: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23],
            datasets: [{
                label: 'kWh / hour',
                data: [0,1,2,3,4,5,6,7,8,9,10,56,12,13,14,15,16,17,18,19,20,21,22,23]
            }]
        }

        data['datasets'].forEach(set => {
            for(let i = 0; i < 24; i++){
                set['data'][i] = Math.random() * 101
            }
        })

        this.setState({data: data});
    }

    fetchPersons() {
        API_DEVICE.getDevices().then(result => {
            this.setState({
                tableData: result,
                isLoaded: true
            });
        }).catch(error => {
            NotificationManager.error('Request to get people failed', error.status + " " + error.statusText, 5000);
        });
    }

    toggleForm(reset = true) {
        this.setState({selected: !this.state.selected});
        if (reset) {
            this.setState({
                userData: {}
            })
        }
    }

    reload(toggleForm = true) {
        this.setState({
            isLoaded: false,
            selectedDevice: null,
            date: null,
        });
        if (toggleForm) {
            this.toggleForm();
        }
        this.fetchPersons();
    }

    deleteUser(userId) {
        API_DEVICE.deleteDevice(userId).then(() => {
            NotificationManager.info(null, "Successfully deleted", 5000);
            this.reload(false);
        }).catch(error => {
            NotificationManager.error('Request to delete user failed', error.status + " " + error.statusText, 5000);
        })
    }

    editUser(user) {
        this.setState({
            userData: user
        });
        this.toggleForm(false);
    }

    showUsage(device) {
        API_DEVICE.getUsage(device.id)
            .then(result => {
                this.setState({
                    selectedDevice: device,
                    deviceUsage: result
                })
            })
    }

    dateChange(data) {
        this.setState({
            date: data,
        })

        const stringDate = data.getFullYear() + "" + ("0"+(data.getMonth()+1)).slice(-2) + "" + ("0" + data.getDate()).slice(-2)
        API_MONITORED_DATA.getDailyUsage(this.state.selectedDevice.id, stringDate).then(result => {
            //this.randomizeData()
            let data = {
                labels: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23],
                datasets: [{
                    label: 'kWh / hour',
                    data: [0,1,2,3,4,5,6,7,8,9,10,56,12,13,14,15,16,17,18,19,20,21,22,23]
                }]
            }

            for (const [key, value] of Object.entries(result)) {
                data['datasets'].forEach(set => {
                    set['data'][key] = value
                })
            }

            this.setState({data: data});
        })
    }

    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Device Management </strong>
                </CardHeader>
                <Card>
                    {
                        this.state.isAdmin &&
                        <Container>
                            <br/>
                            <Row>
                                <Col sm={{size: '12', offset: 0}}>
                                    <Button color="primary" onClick={this.toggleForm}>Add Device </Button>
                                </Col>
                            </Row>
                        </Container>
                    }
                    <br/>
                    <Row>
                        <Col sm={{size: '12', offset: 0}}>
                            {this.state.isLoaded && <DeviceTable
                                deleteHandler={this.deleteUser}
                                editHandler={this.editUser}
                                showUsage={this.showUsage}
                                tableData={this.state.tableData}
                                props={this.props}
                            />}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />}
                        </Col>
                    </Row>
                    {
                        this.state.selectedDevice &&
                        <Row style={{
                            width: "1000px",
                            margin: "2rem auto"
                        }} key={this.state.selectedDevice}>
                            {"Total energy consumption:" + this.state.deviceUsage }
                            <br/>
                            {"Select day: "}
                        <DatePicker
                            selected={this.state.date}
                            onChange={(date) => this.dateChange(date)}
                        />
                            {this.state.date &&
                            <Bar data={this.state.data} type={'bar'}/>
                            }
                        </Row>
                    }
                </Card>

                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Add Device: </ModalHeader>
                    <ModalBody>
                        <DeviceForm userOptions={this.userOptions} data={() => this.state.userData}
                                    reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>

            </div>
        )

    }
}


export default DeviceContainer;
