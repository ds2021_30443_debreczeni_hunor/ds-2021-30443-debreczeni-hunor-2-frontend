import React from "react";
import Table from "../../../commons/tables/table";
import {Link} from "react-router-dom";
import {Container} from "reactstrap";
import Button from "react-bootstrap/Button";
import {authenticationService} from "../../../services";

class DeviceTable extends React.Component {

    columns = [
        {
            Header: "ID",
            accessor: "id",
        },
        {
            Header: 'Name',
            accessor: 'name',
        },
        {
            Header: "User",
            Cell: ({original}) => (
                <Link to={"/users?id=" + original.userId}
                      className="btn btn-outline-primary mr-1">{original.userEmail}</Link>
            )
        },
        {
            Header: 'Actions',
            Cell: ({original}) => (
                this.getCell(original)
            )
        }
    ];

    filters = [
        {
            accessor: 'id',
        },
        {
            accessor: 'name',
        },
        {
            accessor: 'userEmail',
        }
    ];

    constructor(props) {
        super(props);

        const query = new URLSearchParams(this.props.props.location.search);
        this.filters.forEach(filter => {
            let value = query.get(filter.accessor);
            if (value !== undefined) {
                filter['value'] = value;
            }
        })

        this.state = {
            tableData: this.props.tableData,
            isAdmin: authenticationService.isAdmin()
        };

        this.deleteHandler = this.props.deleteHandler;
        this.editHandler = this.props.editHandler;
        this.showUsage = this.props.showUsage;

        this.delete = this.delete.bind(this);
        this.edit = this.edit.bind(this);
        this.select = this.select.bind(this);
    }

    getCell(original) {
        return <Container>
            <Link to={"/sensors?deviceName=" + original.name} className="btn btn-outline-primary mr-1">Sensors
                ({original.sensors.length})</Link>
            {this.state.isAdmin && <Button className={"btn btn-primary mr-1"} onClick={() => this.edit(original)}>Edit</Button> }
            {this.state.isAdmin && <Button className={"btn btn-danger"} onClick={() => this.delete(original)}>Delete</Button>}
            <Button className={"btn btn-info"} onClick={() => this.select(original)}>Select</Button>
        </Container>
    }

    delete(data) {
        this.deleteHandler(data.id);
    }

    edit(data) {
        this.editHandler(data);
    }

    select(data) {
        this.showUsage(data);
    }

    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={this.columns}
                search={this.filters}
                pageSize={5}
            />
        )
    }
}

export default DeviceTable;