import React from 'react'
import logo from './commons/images/icon.png';

import {Nav, Navbar, NavbarBrand, NavLink} from 'reactstrap';
import {authenticationService} from "./services";

const textStyle = {
    color: 'white',
    textDecoration: 'none'
};

const NavigationBar = () => (
    <div>
        <Navbar color="dark" light expand="md">
            <NavbarBrand href="/">
                <img alt={""} src={logo} width={"35"}
                     height={"35"}/>
            </NavbarBrand>
            { authenticationService.getToken() &&
                <Nav className="mr-auto" navbar>
                    { authenticationService.isAdmin() && <NavLink style={textStyle} href="/users">Users</NavLink>}
                    <NavLink style={textStyle} href="/devices">Devices</NavLink>
                    <NavLink style={textStyle} href="/sensors">Sensors</NavLink>
                </Nav>
            }
            <Nav navbar>
                { !authenticationService.getToken() && <NavLink style={textStyle} href="/login">Login</NavLink>}
                { authenticationService.getToken() && <NavLink style={textStyle} href="/logout">Logout</NavLink>}
            </Nav>
        </Navbar>
    </div>
);

export default NavigationBar
