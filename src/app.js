import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import NavigationBar from './navigation-bar'
import Home from './home/home';
import UserContainer from './components/person/user-container'

import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';
import LoginPage from "./components/login/LoginPage";
import {NotificationContainer, NotificationManager} from 'react-notifications';
import PrivateRoute from "./components/PrivateRoute";
import DeviceContainer from "./components/device/device-container";
import SensorContainer from "./components/sensor/sensor-container";
import {authenticationService, socketService} from "./services";

class App extends React.Component {

    componentDidMount() {
        socketService.connectSocket().then(result => {
            socketService.subscribe("/user/" + result.headers['user-name'] + "/queue/notification", response => {
                NotificationManager.info("Sensor " + JSON.parse(response.body).sensor_id + " measured a value over the peak", "Energy usage over peak");
            })
        }).catch(error => {
            console.error(error)
        })
    }

    render() {

        return (
            <div className={styles.back}>
                <Router>
                    <div>
                        <NavigationBar/>
                        <Switch>

                            <Route
                                exact
                                path='/'
                                component={Home}
                                userRole="ROLE_USER"
                            />

                            <PrivateRoute
                                exact
                                path='/users'
                                component={UserContainer}
                                userRole="ROLE_ADMIN"
                            />

                            <PrivateRoute
                                exact
                                path='/devices'
                                component={DeviceContainer}
                                userRole="ROLE_USER"
                            />

                            <PrivateRoute
                                exact
                                path='/sensors'
                                component={SensorContainer}
                                userRole="ROLE_USER"
                            />

                            <Route
                                exact
                                path='/login'
                                render={() => <LoginPage/>}
                            />

                            <PrivateRoute
                                exact
                                path='/logout'
                                component={({ history}) => {
                                    authenticationService.logout()
                                    history.push('/new-location')
                                    return null
                                }}
                                userRole="ROLE_USER"
                            />

                            {/*Error*/}
                            <Route
                                exact
                                path='/error'
                                render={() => <ErrorPage/>}
                            />

                            <Route render={() => <ErrorPage/>}/>
                        </Switch>
                    </div>
                </Router>
                <NotificationContainer/>
            </div>
        )
    };
}

export default App
