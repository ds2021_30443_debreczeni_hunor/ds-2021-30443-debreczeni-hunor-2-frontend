import { authenticationService } from '../services';

export function handleResponse(result, status, error) {
    const data = result && JSON.parse(result);
    if (error !== 1) {
        if ([401, 403].indexOf(status) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            authenticationService.logout();
            // location.reload(true);
        }

        const error = (result) || status;
        return Promise.reject(error);
    }

    return data;
}