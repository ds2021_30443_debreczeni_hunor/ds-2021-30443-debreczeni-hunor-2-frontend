import * as API_AUTH from "../api/auth-api"

export const authenticationService = {
    login,
    logout,
    checkRole,
    getToken,
    isAdmin
};

async function checkRole(role) {
    return await API_AUTH.checkRole(role)
        .then(result => {
            return result
        })
        .catch(() => false);
}

function login(username, password) {
    return API_AUTH.login(username, password)
        .then(user => {
            localStorage.setItem('currentUser', JSON.stringify(user));

            return user;
        });
}

function isAdmin(){
    let parse = JSON.parse(localStorage.getItem('currentUser'));
    return parse ? parse.roles.includes("ROLE_ADMIN") : false;
}

function getToken() {
    let parse = JSON.parse(localStorage.getItem('currentUser'));
    return parse ? parse.token : null;
}

function logout() {
    // remove user from local storage to log out
    localStorage.removeItem('currentUser');
    window.location.href = '/login';
}