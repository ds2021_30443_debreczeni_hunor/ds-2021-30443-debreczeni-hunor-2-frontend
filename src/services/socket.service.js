import SockJS from "sockjs-client";
import Stomp from "webstomp-client";
import {authenticationService} from "./authentication.service";
import {serverEndpoint} from "../constants";

let state = {
    stompClient: null,
    callbackFn: null,
    isConnected: false,
    isSubscribed: false,
}

export const socketService = {
    connectSocket,
    subscribe
};

function connectSocket() {
    if (state.isConnected) {
        return new Promise((resolve) => resolve("Already connected"))
    }

    let token = authenticationService.getToken();
    if (token === null) {
        return new Promise((resolve, reject) => reject("Can't connect to socket, not logged in"))
    }

    const socket = new SockJS(serverEndpoint + '/ws/connect');
    const stompClient = Stomp.over(socket, {
        debug: true,
        heartbeat: {
            outgoing: 10000,
            incoming: 10000
        },
        version: "1.2"
    });
    stompClient.debug = function(str) {};

    let headers = {
        "X-Authorization": token,
    }

    return new Promise(function (resolve, reject) {
        stompClient.connect(headers,
            (message) => {
                state.isConnected = true;
                state.stompClient = stompClient;
                resolve(message)
            },
            (error) => {
                state.isConnected = false;
                state.isSubscribed = false;
                reject(error)
            }
        );
    })
}

function subscribe(url, callbackFn = null) {
    if (!state.isConnected) {
        return;
    }
    if (callbackFn !== null) {
        state.callbackFn = callbackFn;
    }
    if (state.isSubscribed === true) {
        return;
    }
    state.stompClient.subscribe(url, data => {
        if (state.callbackFn !== null) {
            state.callbackFn(data)
        }
    }, error => {
        console.error("error at subscribe", error)
    })

    state.isSubscribed = true;
}