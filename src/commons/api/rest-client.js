import {authenticationService} from '../../services';

function performRequest(request) {
    const token = authenticationService.getToken();
    if (token) {
        request.headers.append("Authorization", 'Bearer ' + token);
    }
    return fetch(request)
        .then(
            function (response) {
                if (response.ok) {
                    return response.json();
                }

                switch (response.status) {
                    case 401:
                        authenticationService.logout();
                        break;
                    case 403:
                        console.info(response);
                        break;
                    default:
                        break;
                }

                return Promise.reject(response);
            })
}

export default {
    performRequest
}