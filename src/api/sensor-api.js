import RestApiClient from "../commons/api/rest-client";
import {serverEndpoint} from "../constants";

const endpoint = {
    sensor: serverEndpoint + '/api/sensor'
};

function getSensors() {
    let request = new Request(endpoint.sensor, {
        method: 'GET',
    });

    return RestApiClient.performRequest(request);
}

function deleteSensor(userId) {
    let request = new Request(endpoint.sensor + "/" + userId, {
        method: 'DELETE',
    });

    return RestApiClient.performRequest(request);
}

function updateSensor(user){
    let request = new Request(endpoint.sensor , {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    return RestApiClient.performRequest(request);
}

function getSensorById(id){
    let request = new Request(endpoint.sensor + id, {
        method: 'GET'
    });

    return RestApiClient.performRequest(request);
}

function postSensor(user){
    let request = new Request(endpoint.sensor , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    return RestApiClient.performRequest(request);
}

export {
    getSensors,
    updateSensor,
    deleteSensor,
    getSensorById,
    postSensor
};
