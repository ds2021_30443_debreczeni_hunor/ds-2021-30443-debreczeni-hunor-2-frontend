import RestApiClient from "../commons/api/rest-client";
import {serverEndpoint} from "../constants";

const endpoint = {
    login: serverEndpoint + '/api/auth/signin',
    roles: serverEndpoint + '/api/user/roleCheck',
};

function login(username, password) {
    let request = new Request(endpoint.login, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({username, password})
    });

    return RestApiClient.performRequest(request);
}

function checkRole(role) {
    let request = new Request(endpoint.roles + "?roleName="+role, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    });

    return RestApiClient.performRequest(request);
}

export {
    login,
    checkRole
};