import RestApiClient from "../commons/api/rest-client";
import {serverEndpoint} from "../constants";

const endpoint = {
    device: serverEndpoint + '/api/device'
};

function getDevices() {
    let request = new Request(endpoint.device, {
        method: 'GET',
    });

    return RestApiClient.performRequest(request);
}

function deleteDevice(userId) {
    let request = new Request(endpoint.device + "/" + userId, {
        method: 'DELETE',
    });

    return RestApiClient.performRequest(request);
}

function updateDevice(user){
    let request = new Request(endpoint.device , {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    return RestApiClient.performRequest(request);
}

function getDeviceById(id){
    let request = new Request(endpoint.device + id, {
       method: 'GET'
    });

    return RestApiClient.performRequest(request);
}

function postDevice(user){
    let request = new Request(endpoint.device , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    return RestApiClient.performRequest(request);
}

function getUsage(id){
    let request = new Request(endpoint.device + `/${id}/usage`, {
        method: 'GET',
    });

    return RestApiClient.performRequest(request);
}

export {
    getUsage,
    deleteDevice,
    updateDevice,
    getDevices,
    getDeviceById,
    postDevice
};
