import RestApiClient from "../commons/api/rest-client";
import {serverEndpoint} from "../constants";

const endpoint = {
    data: serverEndpoint + '/api/data/daily-usage'
};

function getDailyUsage(id, date) {
    let request = new Request(endpoint.data + `/${id}/${date}`, {
        method: 'GET',
    });

    return RestApiClient.performRequest(request);
}

export {
    getDailyUsage
};
