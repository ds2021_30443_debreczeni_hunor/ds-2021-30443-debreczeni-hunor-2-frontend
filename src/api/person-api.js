import RestApiClient from "../commons/api/rest-client";
import {serverEndpoint} from "../constants";

const endpoint = {
    person: serverEndpoint + '/api/user'
};

function getPersons() {
    let request = new Request(endpoint.person, {
        method: 'GET',
    });

    return RestApiClient.performRequest(request);
}

function deleteUser(userId) {
    let request = new Request(endpoint.person + "/" + userId, {
        method: 'DELETE',
    });

    return RestApiClient.performRequest(request);
}

function getPersonById(params){
    let request = new Request(endpoint.person + params.id, {
       method: 'GET'
    });

    return RestApiClient.performRequest(request);
}

function postPerson(user){
    let request = new Request(endpoint.person , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    return RestApiClient.performRequest(request);
}

function updatePerson(user){
    let request = new Request(endpoint.person , {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    return RestApiClient.performRequest(request);
}

export {
    getPersons,
    getPersonById,
    updatePerson,
    postPerson,
    deleteUser
};
