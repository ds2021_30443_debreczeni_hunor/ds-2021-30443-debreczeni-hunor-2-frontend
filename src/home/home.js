import React from 'react';

import {Button, Container, Jumbotron} from 'reactstrap';

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1920px",
};
const textStyle = {
    // color: 'white'
};

const Home = () => {

    return (
        <div>
            <Jumbotron fluid style={backgroundStyle}>
                <Container fluid>
                    <h1 className="display-3" style={textStyle}>Integrated Energy Monitoring
                        Platform for Households</h1>
                    <p className="lead" style={textStyle}><b>Sensor Monitoring System and Real-Time Notification.</b>
                    </p>
                    <hr className="my-2"/>
                    <p style={textStyle}><b>This assignment represents the first module of the distributed software
                        system "Integrated Energy Monitoring
                        Platform for Households" that represents the final project
                        for the Distributed Systems course. </b></p>
                    <p className="lead">
                        <Button color="primary"
                                onClick={() => window.open('https://dsrl.eu/courses/sd/materials/p1.pdf')}>Learn
                            More</Button>
                    </p>
                </Container>
            </Jumbotron>

        </div>
    )
}

export default Home
